const $document = $(document);

$(window).on('load', () => windowLoad());


function windowLoad() {
    emptyLinks();
    externalLinks();
}

function emptyLinks() {
    const links = $('a[href=""], a[href="#"]').toArray();
    console.log('There are ${links.length} empty links on this page', links);
}

function externalLinks() {
    const $links = $('a[href]').filter((i, el) => {
        let href = $(el).attr('href').trim();

        // url doesn't have a protocol, let's add it
        if (href.indexOf('//') === 0) {
            href = document.location.protocol + href;
        }

        const isEmpty = href === '';
        const isRelative = href.indexOf('http://') === -1 || href.indexOf('https://') === -1;
        const isAnchor = href.indexOf('#') === 0;
        const isLocal = isRelative || href.indexOf(document.location.origin) === 0;

        if (isEmpty || isLocal || isAnchor) {
            return false;
        }

        return true;
    });
    $links.attr('target', '_blank');
    console.log('There are ${$links.length} external links on this page', $links);
}