$(document).ready(function() {

	const $bar = $('.ScrollProgress');

	if (!$bar.length) {
		return;
	}

	const $document = $(document);
	const $window   = $(window);
	let height      = $document.height() - $window.height();

	$window.on('scroll.scroll-progress', () => {
		const percent = (window.pageYOffset / height) * 100;
		$bar.css('width', `${percent}%`);
	});

	$window.on('resize.scroll-progress', () => {
		height = $document.height() - $window.height();
		$window.trigger('scroll.scroll-progress');
	});
}); // end ready()