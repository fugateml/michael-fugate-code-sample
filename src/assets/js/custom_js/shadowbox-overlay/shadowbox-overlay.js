$(document).ready(function(){
    $('.shadowbox-btn').on('click',function(){
        var dataIdContent = $(this).attr("data-id");

        console.log(dataIdContent);

        var contentId = $('#shadowbox-overlay').find($('.content'));

        contentId.html("");

        console.log("EMPTY Content: "+contentId.html());

        contentId.html('<img src="'+dataIdContent+'">');

        console.log("Portfolio Image Content: "+contentId.html());
        
        if(contentId.html() != "") {
            $('#shadowbox-overlay').show();
        }
        else {
            $('#shadowbox-overlay').hide();
        }
    });


    $('.closebtn').on('click',function(){
        var contentId = $('#shadowbox-overlay').find($('.content'));
        contentId.html("");
        $('#shadowbox-overlay').hide();
    });

    $('.overlay').on('click',function(){
        var contentId = $('#shadowbox-overlay').find($('.content'));
        contentId.html("");
        $('#shadowbox-overlay').hide();
    });
});