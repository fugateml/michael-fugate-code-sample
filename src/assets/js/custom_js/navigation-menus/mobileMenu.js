$(document).ready(function() {

		const $document  = $(document);
		const $body      = $(document.body);

		$document.on('click', '.js-mobileMenuToggle', (e) => {
			e.preventDefault();
			$body.toggleClass('Body--mobileMenuEnabled');
			console.log = "Body--mobileMenuEnabled class added";
			AriaTag ();
		});

		$document.on('keydown', '.js-mobileMenuToggle', (e) => {
			e.preventDefault();

			if(e.which == 13 || e.which == 32) {
				$body.toggleClass('Body--mobileMenuEnabled');
				console.log = "Body--mobileMenuEnabled class added";
				AriaTag ();
			}
		});



		function AriaTag (){
			if($body.hasClass('Body--mobileMenuEnabled')) {
				$( ".MobileMenu" ).attr( "aria-hidden", "false" );
				$( "nav" ).attr( "aria-hidden", "true" );

				$( ".js-mobileMenuToggle" ).attr( "aria-expanded", "true" );

				/*Change focus from main website to div containing the
				  mobile menu layout */

				 $( ".MobileMenu-content" ).focus();

			}
			else {
				$( ".MobileMenu" ).attr( "aria-hidden", "true" );
				$( "nav" ).attr( "aria-hidden", "false" );
				$( ".js-mobileMenuToggle" ).attr( "aria-expanded", "false" );
			}
		}
}); // end ready()