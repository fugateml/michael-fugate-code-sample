$(document).ready(function() {
    
	/* Toggles Aria Tag Visibility for Main Nav Menu and Mobile Nav Menu */
	
	if($( window ).width() > 800) {
		$( "#nav-bar" ).attr( "aria-hidden", "false" );
		$( "#nav-bar-mobile" ).attr( "aria-hidden", "true" );
	}
	else if($( window ).width() < 801)  {
		$( "#nav-bar" ).attr( "aria-hidden", "true" );
		$( "#nav-bar-mobile" ).attr( "aria-hidden", "false" );
	}


    $(document).on('click', '#nav-bar li', function() {

	   const linkValue = $(this).find('span').attr('src');
	   location.href= linkValue;
	   //alert(linkValue);
	   //console.log(a.attr('href'));
    });

    $(document).on('keydown', '#nav-bar li', function(event){

        if(event.which == 13 || event.which == 32) {
			const linkValue = $(this).find('span').attr('src');
			location.href= linkValue;
			//alert(linkValue);
			//console.log(a.attr('href'));
        }

    });

}); // end ready()