$(document).ready(function() {
    
    $(document).on('click', '.main-nav__mobile-hamburger', (e) => {
        e.preventDefault();
        //alert("READY");
        $(".main-nav__mobile-flyout").toggleClass('active');
    });

    $(document).on('keydown', '.main-nav__mobile-hamburger', (e) => {
        e.preventDefault();

        if(e.which == 13 || e.which == 32) {
            //alert("READY");
            $(".main-nav__mobile-flyout").toggleClass('active');
        }
    });
      
});