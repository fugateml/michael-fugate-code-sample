$(document).ready(function() {
    
    $(document).on('click', '.landingpage-nav__mobile-hamburger', (e) => {
        e.preventDefault();
        //alert("READY");
        $(".landingpage-nav__mobile-flyout").toggleClass('active');
    });

    $(document).on('keydown', '.landingpage-nav__mobile-hamburger', (e) => {
        e.preventDefault();

        if(e.which == 13 || e.which == 32) {
            //alert("READY");
            $(".landingpage-nav__mobile-flyout").toggleClass('active');
        }
    });
      
});