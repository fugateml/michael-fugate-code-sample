$(document).ready(function() {
    
	/* Toggles Aria Tag Visibility for Main Nav Menu and Mobile Nav Menu */
	
	if($( window ).width() > 800) {
		$( "#nav-bar" ).attr( "aria-hidden", "false" );
		$( "#nav-bar-mobile" ).attr( "aria-hidden", "true" );
	}
	else if($( window ).width() < 801)  {
		$( "#nav-bar" ).attr( "aria-hidden", "true" );
		$( "#nav-bar-mobile" ).attr( "aria-hidden", "false" );
	}


    $(document).on('click', '#nav-bar li', function() {

	   const linkValue = $(this).find('span').attr('src');
	   location.href= linkValue;
	   //alert(linkValue);
	   //console.log(a.attr('href'));
    });

    $(document).on('keydown', '#nav-bar li', function(event){

        if(event.which == 13 || event.which == 32) {
			const linkValue = $(this).find('span').attr('src');
			location.href= linkValue;
			//alert(linkValue);
			//console.log(a.attr('href'));
        }

    });

    //languageSelector();


}); // end ready()

function languageSelector(){

	//let devHomePath = "localhost:8000/";
    //let normalPath = "/";
    let currentURL = $(location).attr('href');
    let newURL = "";
    let pageLang = 'en';
	let protocal = "";
	let pageURL = "";
	let homeURL = "/"

	if (!~currentURL.indexOf("/fr/")) {
		pageLang = 'en';
		homeURL = '/fr/';

	} else {
		pageLang = 'fr';
		homeURL = '/';
	}

	newURL = currentURL.split("/");
	protocal = newURL[0] + "//";

	//newURL = (currentURL.substring(1));
	//alert (newURL[2]);

	if (~newURL[2].indexOf("localhost")) {
		newURL.splice(0, 1); //protocal
		newURL.splice(0, 1); // empty array item leftover from "/" array split
		newURL.splice(0, 1); // empty array item leftover from "/" array split
		newURL.splice(0, 1); // page URL
		if (newURL == '') {
			pageURL = newURL;
			homeURL = '/';
		}
		else {
			newURL.splice(1, 1);
			pageURL = newURL;
		}

		alert (homeURL + pageURL);
	}
	else {
		newURL.splice(0, 1); //protocal
		newURL.splice(0, 1); // empty array item leftover from "/" array split
		newURL.splice(0, 1); // empty array item leftover from "/" array split
		newURL.splice(0, 1); // page URL
		if (newURL == '') {
			pageURL = newURL;
			homeURL = '/';
		}
		else {
			newURL.splice(1, 1);
			pageURL = newURL;
		}

		alert (homeURL + pageURL);

	}

}