$(document).ready(function() {

	const $containers = $('.Collapsible');

	if (!$containers.length) {
		return;
	}

	$containers.each((i, container) => {
		const $container = $(container);
		$container.on('click', '.js-toggle', (e) => {
			e.preventDefault();
			const isClosed = $container.hasClass('is-closed');
			if (isClosed) {
				
				/* Closes all Accordian content first */
				$containers.each(function() {
	                $(this).removeClass('is-open');
					$(this).addClass('is-closed');
					$(this).find( ".js-toggle" ).attr('aria-expanded', 'false');
					$(this).find( ".js-toggle" ).removeAttr('aria-disabled');
					$(this).find(".Layout-faqItemContent").attr('aria-hidden', 'true');
                });

				/* Opens Only the Accordian content that was Clicked */
				$container.removeClass('is-closed');
				$container.addClass('is-open');
				$container.find(".js-toggle").attr('aria-expanded', 'true');
				$container.find(".js-toggle").attr('aria-disabled', 'true');
				$container.find(".Layout-faqItemContent").attr('aria-hidden', 'false');

				
			} else {
				$container.removeClass('is-open');
				$container.addClass('is-closed');
				$container.find( ".js-toggle" ).attr('aria-expanded', 'false');
				$container.find( ".js-toggle" ).removeAttr('aria-disabled');
				$container.find(".Layout-faqItemContent").attr('aria-hidden', 'true');
			}
		});

	});
}); // end ready()